﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace MonoCrab
{
    class CTransform : Component
    {
        public Vector2 position { get; set; }

       
        public CTransform(GameObject gameObject, Vector2 position) : base(gameObject)
        {
            this.position = position;
            
        }

        public void Translate(Vector2 translation)
        {
            position += translation;
        }
    }
}
